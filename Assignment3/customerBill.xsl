<?xml version="1.0" encoding="UTF-8"?>
<!--Author: Kennard S - Assignment 3-->
<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema" 
	xmlns:fn="http://www.w3.org/2005/xpath-functions" 
	xmlns:math="http://www.w3.org/2005/xpath-functions/math" 
	xmlns:array="http://www.w3.org/2005/xpath-functions/array" 
	xmlns:map="http://www.w3.org/2005/xpath-functions/map" 
	xmlns:xhtml="http://www.w3.org/1999/xhtml" 
	xmlns:err="http://www.w3.org/2005/xqt-errors" 
	exclude-result-prefixes="array fn map math xhtml xs err" version="3.0">
	<!-- Great work here, Kennard.  Your XSL properly formats and outputs the xml data.
You've also got alternating row colours happening!
10/10
-->
	<xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/" name="xsl:initial-template">
		<html>
			<head>
				<title>Customer Bill</title>
			</head>
			<body>
				<h2>Customer Info</h2>
				Name: <xsl:value-of select="telephoneBill/customer/name" />
				<br/>
				Address: <xsl:value-of select="telephoneBill/customer/address" />
				<br/>
				City: <xsl:value-of select="telephoneBill/customer/city" />
				<br/>
				Province: <xsl:value-of select="telephoneBill/customer/province" />
				<br/><br/>
				<table border="2" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<th>Called Number</th>
							<th>Date</th>
							<th>Duration in Minutes</th>
							<th>Charge</th>
						</tr>
						<xsl:for-each select="telephoneBill/call">
						<tr>
							<xsl:if test="position() mod 2=0">
								<xsl:attribute name="bgcolor">#EAEAEA</xsl:attribute>
							</xsl:if>
							<td><xsl:value-of select="@number" /></td>
							<td><xsl:value-of select="@date" /></td>
							<td><xsl:value-of select="@durationInMinutes" /></td>
							<td><xsl:value-of select="@charge" /></td>

						</tr>
						</xsl:for-each>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
